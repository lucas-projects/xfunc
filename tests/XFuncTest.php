<?php

declare(strict_types=1);

include_once 'src\XFunc.php';

use lucatomasi\XFunc\XFunc;
use PHPUnit\Framework\TestCase;


final class XFuncTest extends TestCase {

    public function test(): void {
        XFunc::addCSSFiles(['css/testFile']);
//        var_dump(XFunc::getCSSFilesAsHTML());

        $this->assertEquals('test', 'test');
    }

}