<?php

namespace lucatomasi\XFunc;

/**
 * @author Luca Tomasi | lucatomasi77@gmail.com
 * @version 3.0.7
*/

/**
 * Class XFunc
 * @package lucatomasi\XFunc
 */
class XFunc {

    /**
     * @var array Holder for CSS file paths
     */
    private static $cssFiles = [];

    /**
     * @var array Holder for JavaScript file paths
     */
    private static $jsFiles = [];

    /**
     * @var array Holds the timers that were started
     */
    private static $executionTimeMeasurements = [];



    /**
     * XFunc constructor.
     */
    private function __construct() {}

    /**
     * Stores the file paths of multiple CSS files
     * @param array $fileURLs An array of CSS file paths
     */
    public static function addCSSFiles(array $fileURLs): void {
        self::$cssFiles = array_merge(self::$cssFiles, $fileURLs);
    }

    public static function isPathRemote($path): bool {
        return (parse_url($path, PHP_URL_HOST) !== NULL);
    }

    /**
     * Returns an array of CSS file paths which were added previously
     * @return array An array of CSS file paths
     */
    public static function getCSSFiles(): array {
        return self::$cssFiles;
    }

    /**
     * Returns html CSS links of the previously added file paths
     * @param bool $debug Describes if page is in debug mode
     * @return string
     */
    public static function getCSSFilesAsHTML(bool $debug=false): string {
        $html = '';
        $t = ($debug) ? '?t=' . time() : '';
        foreach (self::$cssFiles as $cssFile) {
            if (!XFunc::endsWith($cssFile, '.css')) $cssFile .= '.css';
            $html .= "<link rel='stylesheet' href='{$cssFile}{$t}'>";
        }
        return $html;
    }

    public static function addJSFiles(array $fileURLs): void {
        self::$jsFiles = array_merge(self::$jsFiles, $fileURLs);
    }

    /**
     * Returns an array of JavaScript files which were added previously
     * @return array
     */
    public static function getJSFiles(): array {
        return self::$jsFiles;
    }

    /**
     * Returns html script tags of the previously added file paths
     * @param bool $debug Describes if page is in debug mode
     * @return string
     */
    public static function getJSFilesAsHTML(bool $debug = false): string {
        $html = '';
        $t = ($debug) ? '?t=' . time() : '';
        foreach (self::$jsFiles as $jsFile) {
            if (!XFunc::endsWith($jsFile, '.js')) $jsFile .= '.js';
            $html .= "<script src='{$jsFile}{$t}'></script>";
        }
        return $html;
    }

    /**
     * Returns the url and path of the passed file
     * @param $filePath String The Path of a file
     * @return string[] An array of strings containing the path and url of the file path passed
     */
    public static function getPathOf(string $filePath): array {
        $dir = dirname($filePath) . "/";
        $documentRoot = (substr(str_replace("\\", "/", $_SERVER['DOCUMENT_ROOT']), -1) == "/") ? substr($_SERVER['DOCUMENT_ROOT'], 0, strlen($_SERVER['DOCUMENT_ROOT']) - 1) : $_SERVER['DOCUMENT_ROOT'];
        $urlRoot = ((self::usingHTTPS()) ? 'https' : 'http').'://'.$_SERVER['SERVER_NAME'].str_replace(str_replace("\\", "/", $documentRoot), "", str_replace("\\", "/", $dir));
        $url = $urlRoot . ((substr($urlRoot, -1) != "/") ? "/" : "");
        return ['url' => $url, 'dir' => $dir];
    }

    /**
     * Checks if request is via HTTPS
     * @return bool
     */
    public static function usingHTTPS(): bool {
        return (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off')
            || $_SERVER['SERVER_PORT'] == 443;
    }

    /**
     * Generates a random string with the given length, also includes numbers
     * @param int $length The length of the returned string
     * @param bool $allowNumbers If the string can contain numbers
     * @return string The generated string
     */
    public static function getRandomString(int $length = 1, bool $allowNumbers = true): string {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        if ($allowNumbers) $chars .= '0123456789';
        return substr(str_shuffle(str_repeat($x=$chars, ceil($length/strlen($x)))),1, $length);
    }

    /**
     * Checks if a string contains numbers
     * @param string $str The string to be checked
     * @return false|int
     */
    public static function hasNumbers(string $str) {
        return preg_match('~[0-9]+~', $str);
    }

    /**
     * Checks if a string starts with another string
     * @param string $haystack
     * @param string $needle
     * @return bool
     */
    public static function startsWith(string $haystack, string $needle): bool {
        return substr($haystack, 0, strlen($needle)) === $haystack;
    }

    /**
     * Checks if a string ends with another string
     * @param string $haystack
     * @param string $needle
     * @return bool
     */
    public static function endsWith(string $haystack, string $needle): bool {
        return substr_compare($haystack, $needle, -strlen($needle)) === 0;
    }

    /**
     * Removes special characters from a string and replaces spaces with an underscore
     * @param string $str
     * @return string
     */
    public static function generalizeString(string $str): string {
        $new = '';
        foreach (explode(' ', strtolower(preg_replace('/[^A-Za-z0-9_\-]/', ' ', $str))) as $index => $s) {
            $new .= ($index === 0) ? lcfirst($s): ucfirst($s);
        }
        return $new;
    }

    /**
     * Returns a file size limit in bytes based on the PHP upload_max_filesize and post_max_size
     * @return float|int The file size limit in bytes
     */
    public static function getMaxUploadBytes():float {
        static $maxSize = -1;

        if ($maxSize < 0) {
            // Start with post_max_size.
            $maxPostSize = self::parseSize(ini_get('post_max_size'));
            if ($maxPostSize > 0) $maxSize = $maxPostSize;

            // If upload_max_size is less, then reduce. Except if upload_max_size is
            // zero, which indicates no limit.
            $maxUpload = self::parseSize(ini_get('upload_max_filesize'));
            if ($maxUpload > 0 && $maxUpload < $maxSize) $maxSize = $maxUpload;
        }
        return $maxSize;
    }

    /**
     * Parses the amount of bytes from a string with a data unit eg. 1m (Megabyte) => 1000000 (Bytes)
     * @param string $size The size as a string
     * @return int The amount of bytes calculated
     */
    public static function parseSize(string $size): int {
        $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
        $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
        if ($unit) {
            // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
            return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
        } else {
            return round($size);
        }
    }

    /**
     * Starts the timer
     * @param string $name The name of the timer
     */
    public static function startExecutionTimeMeasuring(string $name = ''): void {
        self::$executionTimeMeasurements[$name] = ['start' => getrusage(), 'end' => NULL];
    }

    /**
     * Ends and returns the timer values in milliseconds
     * @param string $name The name of the timer
     * @return array|null
     */
    public static function endExecutionTimeMeasuring(string $name = ''): ?array {

        if (array_key_exists($name, self::$executionTimeMeasurements)) {
            if (self::$executionTimeMeasurements[$name]['end'] === NULL) self::$executionTimeMeasurements[$name]['end'] = getrusage();

            function getRunTime($end, $start, $index) {
                return ($end["ru_$index.tv_sec"]*1000 + intval($end["ru_$index.tv_usec"]/1000)) -  ($start["ru_$index.tv_sec"]*1000 + intval($start["ru_$index.tv_usec"]/1000));
            }

            return [
                'msInComputation' => getRunTime(getrusage(), self::$executionTimeMeasurements[$name]['start'], 'utime'),
                'msInSystemCalls' => getRunTime(getrusage(), self::$executionTimeMeasurements[$name]['start'], 'stime')
            ];
        }
        return NULL;
    }

    /**
     * Returns the recorded execution measurements
     * @return array
     */
    public static function getExecutionTimeMeasurements(): array {
        return self::$executionTimeMeasurements;
    }

}



